# Programms that I use:
- [ssg5](ssg5.html) for generating this site from markdown
- [rssg](https://www.romanzolotarev.com/rssg.html) the rss feed generator of this site
- git
- neovim
- mypy
- gnu coreutils
