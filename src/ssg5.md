# What is ssg5
[ssg5](https://www.romanzolotarev.com/ssg.html) is a shell script which parses one folder and converts the markdown files to html files,
and adds extra to the markdown file _header.html at the top and _footer.html at the bottom of each page

# Patches for ssg5
[generate-article-list](patches/generate_article_list.patch) by [Wolfgang](https://www.notthebe.ee)
[pandoc-render](patches/render_md_file_pandoc.patch) by SZanko
A patch which uses pandoc for converting e.g. render checkboxes in the html
