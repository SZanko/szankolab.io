# About me
## Hobbies

- Linux
- Programming
- Swimming

## Thing that I can do
<details>
<summary>
Java
</summary>
- JavaFx
- Spring Boot
- Junit
- Hibernate
</details>

<details>
<summary>
C#
</summary>
- WPF
- ASP.NET Core
- Xunit
- EF Core
</details>

<details>
<summary>
Python
</summary>
- pytest
- PyQt
</details>


<details>
<summary>
Typescript
</summary>
- Angular
</details>
