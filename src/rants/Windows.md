# Windows

I hate Windows.
So let's come to my rant about it.
In the past I didn't hate it as much as I do it today.
However after I have used other operating systems than Windows I came to the conclusion that I maybe feel better to write a blog post about the things that I don't like.

## Why I hate Windows

### Non-technical Stuff

#### Bloat

The last time I made a clean install of Windows.
Windows installed a lot of useless programs that I am not going to need at any in the future.
Why is there no option to do a minimal install out of the box without building a custom iso of Windows.
I don't need: a news program, a weather program and many others, in my operating preinstalled.

#### Cortana

Why do I need a virtual assistant which is preinstalled and probably spies on my.
I don't need a personal which writes down my reminders or look for the weather for me.
I can all do this by my self.  

The only positive about the software is that it can be uninstalled from the system via:  
`Get-AppxPackage *Microsoft.549981C3F5F10* -AllUsers| Remove-AppxPackage`  
Copy this into a powershell with admin privileges and run it 

#### Error Sounds

I hate the fucking error of windows this is not really windows specific but I find the motherfucking error beeps on
windows unacceptable. Every time I hear one beep I have to resist to through out the computer out of the window.
Windows peeps to often at me compared to any GNU/Linux Distro with any DE on it installed.  
Windows peeps at me if a try to open a admin powershell prompt or try to change a setting which doesn't work.
Sometimes I have the feeling it beeps every second if i try to click something.

But thanks to none existing god there is also a for this problem which drives my nuts.  
Just go to the System Control and under the sounds settings there should be a option to turn it off.

#### Updates and Package management 
The first thing that I have to say that I think that Microsoft is right with their decision to force everybody to do their updates.
You wouldn't fuck some with a broken condom so you shouldn't use a computer without its security updates.
In the past some people didn't do their security updates so Microsoft felt forced to turn on updates for everyone.

The think that I don't like about the updates that every piece software has its own updater.
Even Microsoft has a separate updater for Visual Studio, and their office suite.
Why not have a Windows Update Integration for even third party devs similar to PPAs on Debian and Ubuntu.
It would make it so much easier for devs to get their users to use the newest Version.

So let's come to the other topic of the heading.  
Yes I know that microsoft is building a package manager at the moment with the name [winget](https://github.com/microsoft/winget-cli).
But for me it seems that this package manager is mostly focused on developers and it also don't integrate with Windows Updates or Powershell. Why isn't this package manager installed by default in windows, why isn't there a windows store integration.

#### Error Codes
How thought that a fucking hex dump is a good idea to present the error, why are there no details what happened.
Which critical process died it don't know maybe the fucking telemetry process or sent satan claus a email process.
Why is there no info on the blue screen so you have to go and view the logs.

#### Customization 

Why is there no easy way build in way to disable windows shortcuts, without entering the registry and change some fucking obscure value and jump to the next location because the shortcuts aren't located in the same area. I tried to because I wanted to run a tiling window manager in Windows. 

#### Legacy Stuff
Why is EDITOR, Internet Explorer 11 still included with the system.
EDITOR is the worst default editor I have ever dealt with. 
It doesn't have syntax highlighting of any kind of programming language or even HTML which is ridicules. 
It also lacks a search and replace function.


#### Default naming of Folders
Why are there default folders with spaces in the name. This make command line navigation unneasery worse. 



#### Spying

### Technical Stuff

#### Backslashes
I hate backslashes the fucking suck to write on a keyboard with a qwertz layout you have to type  
`alt gr plus the key cap next to 0`  
which I find really uncomfortable compared to pressing for a forward slash  
`shift plus 7`  
For backslashes I have to type this with my right hand alone with forward slashes a have the choice of writing them 

#### Registry

Who thought that it was a good idea to a safe system configurations in a database with some motherfucking binary keys.
I don't want to search on the internet how to edit a fucking cryptic to change to something that i want to do.
I don't know it seems so much easier just to edit a motherfucking configuration file with a text editor in a mostly human readable syntax.

#### 256 character limit
The first time I encountered this absurd limit it nearly drove my insane.
Today I still don't know why microsoft have set this absurd character limit in a 64 bit OS.  
Which opens up another question: Why do I have to go the registry just to enable a setting which should be enabled by default?  
How should I know this without searching only for the fucking registry key with changes this behaviour to a "normal" one?  
Why is there no gui setting which toggles that option for normal users?  

#### Windows Path
Why is the Windows Path so empty if i type firefox I only get the response cmdlet not found. Why aren't exes auto added to the path if the are installed in C:\Programs or the 32bit one? Is there a reason for this decision?

#### Commandline

## Why I like Windows

However, I also like things about windows which I would like to write about
### Proprietary Software
