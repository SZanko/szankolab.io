# szanko.gitlab.io
# See LICENSE file for copyright and license details.

tags:
	ctags -R *

init-linux:
	mkdir -p bin && \
	curl -s https://rgz.ee/bin/ssg > bin/ssg && \
	curl -s https://rgz.ee/bin/rssg > bin/rssg && \
	sed -i 's/ -j/-R/g' bin/rssg && \
    sed '121d' bin/rssg && \
    sed -i '120s/ \\//g' bin/rssg && \
   	sed -i 's/date_rfc_822 "$date"/date -R/g' bin/rssg && \
   	sed -i 's/date_rfc_822 date/date -R/g' bin/rssg && \
	curl -Os https://kristaps.bsd.lv/lowdown/snapshots/lowdown.tar.gz && \
	tar -xzf lowdown.tar.gz && \
	cd lowdown-*/ && \
	./configure && make && \
	cd ../ && cp lowdown-*/lowdown bin/lowdown
	chmod +x bin/ssg bin/lowdown && \
	rm -rf lowdown-*
	echo 'TODO add to Path via make path'

path:
	echo 'Add temporary to path'
	export PATH="${PATH}:$(pwd)/bin"
	echo 'To add lowdown and ssg5 to your path permament write: export PATH="${PATH}:$(pwd)/bin" to your profile'

init-arch:
	sudo pacman -S cpio && \
	yay -S lowdown ssg rssg

clean:
	rm -rf ./public ./ctags

install:
	mkdir -p public/img 
	ssg src public 'SZanko' 'https://szanko.gitlab.io' && \
	rssg public/index.html "SZanko's Blog" > public/rss.xml \

.PHONY: init path install
