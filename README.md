![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---
# What is this
This is my personal plain HTML site using GitLab Pages with [ssg5](https://www.romanzolotarev.com/ssg.html) and [lowdown](https://kristaps.bsd.lv/lowdown/) and [rssg](https://www.romanzolotarev.com/rssg.html).

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

# How to use it
Write your Markdown in the src directory and after that run ````make install```` and make and watch the result in the browser or push it directly to Gitlab. In the future it will also autogenerate a rss feed

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)



The above example expects to put all your HTML files in the `public/` directory.

